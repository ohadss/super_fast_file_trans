# super_fast_file_trans

为鸿蒙应用开发中大文件传输场景打造的一款文件传输三方库（super_fast_file_trans, SFFT），致力于提供多线程并发下载、分片上传、断点续下/传、自动重试等多种文件传输过程中的典型功能特性。

## 简介

大文件高速并发传输三方库，针对大文件传输场景实现了多种功能特性。

## 特性说明

### 多线程分块下载

SFFT提供使用多线程并发下载目标文件的能力，提高传输效率。当启动下载时，若没有设置并发线程数（concurrency），SFFT会对目标文件进行预下载（试连），并根据试连响应头中的结果（Content-Range），根据文件大小自动计算启动的线程数，线程数量的计算规则如下：

| 文件大小范围       | 并发线程数 |
|--------------|-------|
| x<10MB       | 1     |
| 10MB≤x<100MB | 2     |
| 100MB≤x      | 4     |

### 断点续下

SFFT提供下载时断点续下能力，可由开发者自行决定是否启用。当启动下载时，SFFT会实时存储下载进度到本地数据库中，以实现在网络中断、客户端崩溃等场景下能够从文件下载中断的位置继续下载。

### 异步分片上传

SFFT提供分片上传本地文件的能力。当启用分片上传时，若没有设置每个上传请求携带的分片大小（chunkSize），SFFT会根据本地文件大小计算合适的分片，并发送若干请求传输分片大小的数据，分片大小的计算规则如下：

| 文件大小范围 | 分片大小   |
|--------|--------|
| x<1G   | 1MB    |
| 1GB≤x  | x/1000 |

### 断点续传

SFFT提供上传时断点续传能力，可由开发者自行决定是否启用。当启动上传时，SFFT会实时存储上传进度到本地数据库中，以实现在网络中断、客户端崩溃等场景下能够从文件上传中断的位置继续上传。

### 自动重试机制

SFFT提供下载或上传失败时自动重试的能力，可由开发者自行决定重试次数和重试时间间隔。当下载或上传失败时，SFFT会自动尝试重新连接服务器，从断点处继续传输数据。

### 回调机制

SFFT提供下载或上传过程中多个阶段的回调接口，详情见**接口说明**。

## 使用说明

### 安装库

```shell
ohpm install @hadss/super_fast_file_trans
```

### 下载文件

  **1.获取DownloadManager单例对象，初始化数据库。**

  ```typescript
  import { DownloadManager } from '@hadss/super_fast_file_trans';

  const context = getContext(this);

  const downloadManager = DownloadManager.getInstance(); // 获取DownloadManager单例对象

  await downloadManager.init(context); // 初始化数据库
  // ...

  await downloadManager.cleanAll(context); // 删除下载数据库,清除缓存
  ```

  **2.自定义下载配置和下载回调，创建下载任务实例。**

  ```typescript
  import { DownloadConfig, DownloadListener, DownloadTask, DownloadProgressInfo } from '@hadss/super_fast_file_trans';

  // 自定义下载回调
  let customDownloadListener: DownloadListener = {
    onSuccess: () => {
      console.log("onSuccess: download success")
    },
    onFail: (err: BusinessError) => {
      console.error(`onFail: download fail, err.message:${err.message}, err.code:${err.code}`);
    },
    // ...
    onProgressUpdate: (downloadProgress: DownloadProgressInfo) => {
      console.log(`onProgressUpdate:, download: transferred size:${downloadProgress.transferredSize}, total size:${downloadProgress.totalSize}`)
    },
  };

  // 自定义下载配置
  let downloadConfig:DownloadConfig = {
    url: `${YOUR_DOWNLOAD_URL}`, // 远端下载地址（必选）
    fileName: `${YOUR_LOCAL_FILE_NAME}`, // 下载后的本地文件名（必选）
    concurrency: 2, // 启用的并发线程数（可选）
    // ...
  };

  // 创建下载任务
  const downloadTask:DownloadTask = downloadManager.createDownloadTask(downloadConfig, customDownloadListener);
  ```

  **3.DownloadTask功能接口使用：下载/暂停/获取下载进度/恢复/取消。**

  ```typescript
  if (downloadTask) {
    await downloadTask.start();
    await downloadTask.pause();
    let progressInfo:DownloadProgressInfo = await downloadTask.getProgress();
    await downloadTask.resume();
    await downloadTask.cancel();
  }
  ```

### 上传文件

  **1.获取UploadManager单例对象，初始化数据库。**

  ```typescript
  import { UploadManager } from '@hadss/super_fast_file_trans';

  const context = getContext(this);

  const uploadManager = UploadManager.getInstance(); // 获取UploadManager单例对象

  await uploadManager.init(context); // 初始化上传数据库;
  // ...

  await uploadManager.cleanAll(context); // 删除上传数据库,清除缓存
  ```

  **2.自定义上传配置和上传回调，创建上传任务实例。**

  ```typescript
  import { UploadConfig, UploadListener, UploadTask, UploadProgressInfo } from '@hadss/super_fast_file_trans';

  // 自定义上传回调
  let customUploadListener: UploadListener = {
    onSuccess: () => {
      console.log("onSuccess: upload success")
    },
    onFail: (err: BusinessError) => {
      console.error(`onFail: upload fail, err.message:${err.message}, err.code:${err.code}`);
    },
    // ...
    onProgressUpdate: (uploadProgress: UploadProgressInfo) => {
      console.log(`onProgressUpdate:, upload: transferred size:${uploadProgress.transferredSize}, total size:${uploadProgress.totalSize}`)
    }
  };

  // 自定义上传配置
  let fileDir = context.filesDir;

  let uploadConfig:uploadConfig = {
    url: `${YOUR_UPLOAD_SERVER_URL}`, // 远端上传地址（必选）
    filePath: `${FILE_DIR}/${FILE_NAME}`, // 本地上传文件的路径（必选）
    isChunk: false // 是否启用分片上传（可选）
    // ...
  };

  // 创建上传任务
  const uploadTask:UploadTask = uploadManager.createUploadTask(uploadConfig, customUploadListener);
  ```

  **3.UploadTask功能接口使用：上传/暂停/获取上传进度/恢复/取消。**

  ```typescript
  if (uploadTask) {
    await uploadTask.start();
    await uploadTask.pause();
    let progressInfo:UploadProgressInfo = await uploadTask.getProgress();
    await uploadTask.resume();
    await uploadTask.cancel();
  }
  ```

## 接口说明

### 下载文件

**DownloadManager：下载任务管理**

| 接口名称                        | 参数类型                                              | 返回值类型                     | 功能简介                      |
|-----------------------------|---------------------------------------------------|---------------------------|---------------------------|
| DownloadManager.getInstance | NA                                                | DownloadManager           | 获取DownloadManager单例对象     |
| init                        | context:common.Context                            | Promise\<void>            | 初始化断点续下数据库数据到缓存           |
| cleanAll                    | context:common.Context                            | Promise\<void>            | 清空断点续下缓存，删除数据库以未下载完成的临时文件 |
| createDownloadTask          | config:DownloadConfig, listener?:DownloadListener | DownloadTask \| undefined | 获取DownloadTask对象          |

注意：createDownloadTask方法会对config参数中的concurrency、maxRetries、retryInterval进行校验，需满足（1≤concurrency≤8, 0≤maxRetries≤10, 500≤retryInterval≤
10000），否则会返回undefined。

**DownloadTask：下载任务**

| 接口名称        | 参数类型 | 返回值类型                          | 功能简介                                      |
|-------------|------|--------------------------------|-------------------------------------------|
| start       | NA   | Promise\<void>                 | 开启下载任务（正在下载的任务执行该接口无效，执行前会生成临时文件及数据库断点信息） |
| pause       | NA   | Promise\<void>                 | 暂停下载任务（已暂停的下载任务执行该接口无效）                   |
| resume      | NA   | Promise\<void>                 | 恢复下载任务（正在下载的任务执行该接口无效）                    |
| cancel      | NA   | Promise\<void>                 | 取消下载任务（同时会删除临时文件及数据库断点信息）                 |
| getProgress | NA   | Promise\<DownloadProgressInfo> | 获取下载任务进度                                  |

**DownloadProgressInfo：下载进度信息参数**

| 参数名称            | 参数类型   | 功能简介             |
|-----------------|--------|------------------|
| transferredSize | number | 当前下载任务已经下载的字节数   |
| totalSize       | number | 当前下载任务的总字节数      |
| speed           | number | 当前下载任务的下载速率(B/s) |

**DownloadConfig：下载策略参数**

| 参数名称                | 参数类型                                            | 功能简介                                                                     |
|---------------------|-------------------------------------------------|--------------------------------------------------------------------------|
| url                 | string                                          | **必选**，远端下载文件的url                                                        |
| fileName            | string                                          | **必选**，下载文件的本地文件名                                                        |
| fileDir?            | string                                          | 下载文件的文件夹路径，默认为当前应用上下文的fileDir目录                                          |
| concurrency?        | number                                          | 下载时启用的线程数，建议不超过4，默认会根据试连响应头中的文件大小计算线程数                                   |
| maxRetries?         | number                                          | 下载失败时最大重试次数，默认为3                                                         |
| retryInterval?      | number                                          | 下载失败时重试间隔，默认为500ms                                                       |
| isBreakpointResume? | boolean                                         | 是否启用断点续下，默认为true，如不启用，则任务无法暂停和继续                                         |
| isOverWrite?        | boolean                                         | 如指定下载路径存在同名文件，是否覆盖，默认为true，若为false，当出现同名文件时，下载文件会在文件名末尾添加序号，如test(1).txt |
| connectionTimeout?  | number                                          | 下载请求连接的超时值，默认为60000ms                                                    |
| transferTimeout?    | number                                          | 下载传输过程的超时值，默认为600000ms                                                   |
| inactivityTimeout?  | number                                          | 传输数据间隔的超时值，默认为10000ms                                                    |
| requestHeaders?     | Record<string, string \| string[] \| undefined> | 试连和下载请求的需要额外添加的请求头，默认为rcp中fetch函数自带的请求头                                  |
| securityConfig?     | SecurityConfig                                  | 在会话中配置与安全相关的设置，包括证书和服务器身份验证                                              |

**DownloadListener: 下载回调接口**

| 接口名称              | 参数类型                                                                                                              | 返回值类型 | 功能简介                                                                          |
|-------------------|-------------------------------------------------------------------------------------------------------------------|-------|-------------------------------------------------------------------------------|
| onStart?          | trialResponseHeaders:Record<string, string \| string[] \| undefined>                                              | NA    | 下载任务开始时被调用，trialResponseHeaders：试连返回的response信息                               |
| onPause?          | downloadProgressInfo:DownloadProgressInfo                                                                         | NA    | 下载任务暂停时被调用，downloadProgressInfo：下载进度信息                                        |
| onResume?         | trialResponseHeaders: Record<string, string \| string[] \| undefined> , downloadProgressInfo:DownloadProgressInfo | NA    | 下载任务恢复时被调用，trialResponseHeaders：重新试连返回的response信息，downloadProgressInfo：下载进度信息 |
| onCancel?         | NA                                                                                                                | NA    | 下载任务取消时被调用                                                                    |
| onProgressUpdate? | downloadProgressInfo:DownloadProgressInfo                                                                         | NA    | 下载任务进度信息更新时被调用， downloadProgressInfo：下载进度信息                                   |
| onSuccess?        | filePath:string                                                                                                   | NA    | 下载任务成功时被调用，filePath：下载文件路径                                                    |
| onFail?           | error:BusinessError                                                                                               | NA    | 下载任务失败时被调用，error：错误信息                                                         |

### 上传文件

**UploadManager：上传任务管理**

| 接口名称                      | 参数类型                                          | 返回值类型                       | 功能简介                |
|---------------------------|-----------------------------------------------|-----------------------------|---------------------|
| UploadManager.getInstance | NA                                            | UploadManager               | 获取UploadManager单例对象 |
| init                      | context:common.Context                        | Promise\<void>              | 初始化断点续传数据库数据到缓存     |
| cleanAll                  | context:common.Context                        | Promise\<void>              | 清空断点续传缓存、删除数据库      |
| createUploadTask          | config:UploadConfig, listener?:UploadListener | UploadListener \| undefined | 获取UploadTask对象      |

**UploadTask：上传任务**

| 接口名称        | 参数类型 | 返回值类型                        | 功能简介                                   |
|-------------|------|------------------------------|----------------------------------------|
| start       | NA   | Promise\<void>               | 开启上传任务（正在上传的任务执行该接口无效，执行前会生成数据库断点信息数据） |
| pause       | NA   | Promise\<void>               | 暂停上传任务（已暂停的上传任务执行该接口无效）                |
| resume      | NA   | Promise\<void>               | 恢复上传任务（正在上传的任务执行该接口无效）                 |
| cancel      | NA   | Promise\<void>               | 取消上传任务（同时会删除数据库断点信息数据）                 |
| getProgress | NA   | Promise\<UploadProgressInfo> | 获取上传任务进度                               |

**UploadProgressInfo：上传进度信息参数**

| 参数名称            | 参数类型   | 功能简介             |
|-----------------|--------|------------------|
| transferredSize | number | 当前上传任务已经上传的字节数   |
| totalSize       | number | 当前上传任务的总字节数      |
| speed           | number | 当前上传任务的上传速率(B/s) |

**UploadConfig：上传策略参数**

| 参数名称               | 参数类型                                            | 功能简介                                                       |
|--------------------|-------------------------------------------------|------------------------------------------------------------|
| url                | string                                          | **必选**，远端上传文件的url                                          |
| filePath           | string                                          | **必选**，本地上传文件路径                                            |
| uploadFileName?    | string                                          | 上传文件名，请求表单中携带，默认为filePath对应文件的文件名                          |
| contentType?       | number                                          | 上传文件类型，请求表单中携带，默认为undefined                                |
| isChunk?           | boolean                                         | 是否使用分片上传，默认为false                                          |
| chunkSize?         | number                                          | 分片上传时的文件大小(字节)，仅在isChunk为true时启用，如未设置，则使用默认计算规则（见**特性说明**） |
| maxRetries?        | number                                          | 上传失败时最大重试次数，默认为3                                           |
| retryInterval?     | number                                          | 上传失败时重试间隔，默认为500ms                                         |
| connectionTimeout? | number                                          | 上传请求连接的超时值，默认为60000ms                                      |
| transferTimeout?   | number                                          | 上传传输过程的超时值，默认为600000ms                                     |
| inactivityTimeout? | number                                          | 传输数据间隔的超时值，默认为10000ms                                      |
| requestHeaders?    | Record<string, string \| string[] \| undefined> | 上传请求的需要额外添加的请求头，默认会将content-type设置为multipart/form-data     |
| hashConfig?        | HashConfig                                      | 上传请求时哈希相关的配置，包括可选的哈希值、哈希算法以及哈希头名                           |
| securityConfig?    | SecurityConfig                                  | 在会话中配置与安全相关的设置，包括证书和服务器身份验证                                |

注意：createUploadTask方法会对config参数中的chunkSize、maxRetries、retryInterval进行校验，需满足（1M≤chunkSize≤200M, 0≤maxRetries≤10, 500≤retryInterval≤
10000），否则会返回undefined。chunkSize越大，上传过程中应用占用内存越高，需合理设置分片大小。

**UploadListener: 上传回调接口**

| 接口名称              | 参数类型                                  | 返回值类型 | 功能简介                                      |
|-------------------|---------------------------------------|-------|-------------------------------------------|
| onStart?          | NA                                    | NA    | 上传任务开始时被调用                                |
| onPause?          | uploadProgressInfo:UploadProgressInfo | NA    | 上传任务暂停时被调用，uploadProgressInfo：上传进度信息      |
| onResume?         | NA                                    | NA    | 上传任务恢复时被调用，uploadProgressInfo：上传进度信息      |
| onCancel?         | NA                                    | NA    | 上传任务取消时被调用                                |
| onProgressUpdate? | uploadProgressInfo:UploadProgressInfo | NA    | 上传任务进度信息更新时被调用， uploadProgressInfo：上传进度信息 |
| onSuccess?        | filePath:string                       | NA    | 上传任务成功时被调用，filePath：本地上传的文件的地址            |
| onFail?           | error:BusinessError                   | NA    | 上传任务失败时被调用，error：错误信息                     |

**HashConfig：哈希配置**

| 参数名称           | 参数类型                        | 功能简介                                                     |
|----------------|-----------------------------|----------------------------------------------------------|
| hashValue?     | string                      | 待上传文件的哈希值，不配置则默认内部进行计算                                   |
| hashAlgorithm? | 'md5' \| 'sha1' \| 'sha256' | 获取哈希值的相应算法，当前支持md5、sha1以及sha256，不配置则默认为sha256            |
| hashHeader?    | string                      | 传输过程中哈希头的名称，不配置则默认为'X-File-Hash-xxx'，其中xxx为hashAlgorithm |

注意：手动配置hashValue可以在上传文件较大的情况下优化计算哈希值导致的性能问题。

### 会话安全相关配置

**SecurityConfig: 会话安全配置**

| 参数名称              | 参数类型                                       | 功能简介                                         |
|-------------------|--------------------------------------------|----------------------------------------------|
| remoteValidation? | 'system' \| 'skip' \| CertificateAuthority | 用于验证远程服务器的身份,默认值为'system':使用系统校验，'skip':跳过校验 |
| certificate?      | ClientCertificate                          | 发送到远程服务器的客户端证书                               |
| tlsOptions?       | 'system' \| TlsV13Option \| TlsV12Option   | TLS版本选择器，默认为'system':使用系统TLS版本               |

**CertificateAuthority: 证书颁发机构（CA）用于验证远程服务器的身份。**

| 参数名称        | 参数类型                  | 功能简介                                |
|-------------|-----------------------|-------------------------------------|
| content?    | string \| ArrayBuffer | 证书颁发机构（CA）证书的内容。是PEM格式。             |
| filePath?   | string                | 用于验证对等方的证书颁发机构证书文件的路径。该文件应为PEM格式。   |
| folderPath? | string                | 用于验证对等方包含多个CA证书目录的路径。此目录中的文件应为PEM格式 |

**ClientCertificate: 客户端证书配置**

| 参数名称         | 参数类型                    | 功能简介                  |
|--------------|-------------------------|-----------------------|
| content?     | string \| ArrayBuffer   | 证书颁发机构（CA）证书的内容。      |
| filePath?    | string                  | 包含证书颁发机构（CA）证书的文件的路径。 |
| type?        | 'PEM' \| 'DER' \| 'P12' | 客户端证书编码格式类型。          |
| key?         | string                  | 客户端证书的key。            |
| keyPassword? | string                  | 客户端证书密码。              |

**TlsV13Option: TLS1.3选择器，以及配套加密套件**

| 参数名称         | 参数类型              | 功能简介                 |
|--------------|-------------------|----------------------|
| tlsVersion   | string            | 指明TLS版本。默认为'TlsV1.3' |
| cipherSuite? | TlsV13CipherSuite | TLS1.3版本对应的加密套件。     |

type TlsV13CipherSuite = 'TLS_AES_128_GCM_SHA256' | 'TLS_AES_256_GCM_SHA384' | 'TLS_CHACHA20_POLY1305_SHA256'
| TlsV12CipherSuite;

**TlsV12Option: TLS1.2选择器，以及配套加密套件**

| 参数名称         | 参数类型              | 功能简介                 |
|--------------|-------------------|----------------------|
| tlsVersion   | string            | 指明TLS版本。默认为'TlsV1.2' |
| cipherSuite? | TlsV12CipherSuite | TLS1.2版本对应的加密套件。     |

type TlsV12CipherSuite = 'TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256' | 'TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256'
| 'TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384' | 'TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384' | 'TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256'
| 'TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256' | 'TLS_RSA_WITH_AES_128_GCM_SHA256' | 'TLS_RSA_WITH_AES_256_GCM_SHA384';

### 接口异常

当任务失败时会抛出异常，可使用DownloadListener和UploadListener中的onFail回调获取异常。

| 异常码      | 异常信息               |
|----------|--------------------|
| 20000000 | 下载前远端试连连接失败        |
| 20000001 | 下载前试连结果不满足下载连接条件   |
| 20000002 | 远端文件前后发生变化         |
| 20000003 | 当前下载任务缺少分块信息       |
| 20000004 | 下载线程异常终止           |
| 20000005 | 下载待写入文件无法打开或创建失败   |
| 20000006 | 保存已下载文件失败          |
| 21000000 | 初始化下载任务信息失败        |
| 21000001 | 删除（重置）下载任务信息失败     |
| 21000002 | 存储下载任务信息失败         |
| 21000003 | 存储下载任务进度（块）信息失败    |
| 21000004 | 从数据库或缓存获取下载任务信息失败  |
| 21000005 | 从数据库或缓存获取下载块信息失败   |
| 21000006 | 更新数据库下载任务信息到缓存失败   |
| 21000007 | 更新下载任务信息到数据库失败     |
| 21000008 | 通过任务id删除相关下载任务信息失败 |
| 30000000 | 本地待上传文件不存在         |
| 30000002 | 上传响应头状态码错误         |
| 31000000 | 初始化上传数据信息失败        |
| 31000001 | 删除（重置）上传任务信息失败     |
| 31000002 | 存储上传任务进度信息失败       |
| 31000003 | 从数据库或缓存获取上传任务信息失败  |
| 31000004 | 从数据库或缓存获取分片状态信息失败  |
| 31000005 | 获取分片信息失败           |
| 31000006 | 更新上传任务信息到缓存失败      |
| 31000007 | 更新上传任务信息到数据库失败     |
| 31000008 | 通过任务id删除相关上传任务信息失败 |
| 91000002 | 删除本地文件失败           |
| 91000006 | 获取临时文件名失败          |
| 92000000 | URL无效              |
| 92000001 | 连接中断               |
| 93000000 | 断点信息丢失             |
| 93000001 | 本地文件改变             |
| 93000002 | 远端文件改变             |

## 相关权限

SFFT依赖以下权限：<br>

ohos.permission.INTERNET

## 约束与限制

SFFT在以下版本通过:<br>

- DevEco Studio:5.0.1 Beta3 (5.0.5.200)
- SDK: API13 (5.0.1.106)

## 注意事项

### 下载文件

SFFT依赖服务端支持实现分块下载，服务端需满足以下要求:
1. 服务端**必须**支持**Range**请求头，允许请求指定字节范围的数据块。这样请求可以只下载文件的部分内容，而不是整个文件。
2. 服务端响应时，**必须**支持**Content-Range**响应头，并返回指定的字节范围，这告诉请求返回的时哪个字节范围的数据块，并指明了文件的总大小。同时，建议响应头携带Content-Length、Content-Type、Accept-Ranges字段：

```http
Content-Range: bytes 0-999/5000  # 指明返回文件的字节范围，文件总大小为5000字节
Content-Length: 1000 # 响应返回的字节长度
Content-Type: application/octet-stream # 响应体为二进制流
Accept-Ranges: bytes # 指明服务端支持断点续下
```
3. 当接收到携带Range头的请求时，服务端建议支持并返回 **206 Partial Content** 状态码：

```http
HTTP/1.1 206 Partial Content
```
4. 如果服务端希望支持Etag或Last-modified响应头字段来支持文件一致性校验，建议支持**Etag**或**Last-modified**字段，在提供上述字段的情况下试连会在断点续下和继续下载时对响应头中的
   **Etag**和**Last-modified**进行一致性判断，若与之前不一致将无法通过校验，导致请求失败：

```http
Etag:"d41d8cd98f00b204e980099800998ecf8427e"
Last-Modified: Wed, 21 Oct 2023 07:28:00 GMT
```

### 上传文件

SFFT依赖服务端支持实现分片上传，服务端需满足以下要求：
1. 服务端**必须**支持**multipart/form-data**，能够支持文件按和其他表单数据在HTTP协议中一起上传。
2. 服务端**必须**支持接受并保存分片数据。服务端能够接收并解析上传请求中的表单数据，并能够将表单中数据保存到指定位置。使用分片上传的文件的默认表单项为：

```http
totalChunks:xxx
chunkIndex:xxx
file:xxx
```

3. 服务端支持合并分片数据/文件。

4. 服务端支持自定义哈希值字段读取并能够在文件合并后校验哈希值以保证一致性。当前传输头内部哈希值字段默认命名为**X-File-Hash-xxx**，其中**xxx**为算法名，例如**X-File-Hash-sha256**。如果配置了hashHeader则使用用户自定义的命名。

```http
// 默认命名
X-File-Hash-sha256:"C9CC55C19BB1B85B8B3E88BE3B6F824BEFF5C62671F03B6729B59136B9380CC1"
// 用户自定义命名
custom-Hash:"C9CC55C19BB1B85B8B3E88BE3B6F824BEFF5C62671F03B6729B59136B9380CC1"
```

### 会话安全配置

双向校验依赖客户端证书，使用安全配置相关接口前需要在客户端本地安装或使用系统证书。

## 参考资料

[super_fast_file_trans使用案例](https://gitee.com/ohadss/super_fast_file_trans)

## 贡献代码

使用过程中发现任何问题都可以提 [issue](https://gitee.com/ohadss/super_fast_file_trans/issues)给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/ohadss/super_fast_file_trans/pulls) 。

## 开源协议

本项目基于[Apache License 2.0](https://gitee.com/ohadss/super_fast_file_trans/blob/master/LICENSE)，请自由的享受和参与开源。
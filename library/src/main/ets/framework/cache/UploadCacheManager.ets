/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UploadTaskInfo } from '../storage/UploadTaskInfo';
import { CacheError, CacheErrorType } from '../../common/exception/CacheError';
import LoggerConstants from '../../common/constants/LoggerConstants';
import Logger from '../../common/utils/Logger';

export class UploadCacheManager {
  private static instance: UploadCacheManager = new UploadCacheManager();
  private taskCache: Map<number, UploadTaskInfo>;

  private constructor() {
    this.taskCache = new Map<number, UploadTaskInfo>();
  }

  public static getInstance(): UploadCacheManager {
    return UploadCacheManager.instance;
  }

  public clearCache() {
    this.taskCache.clear();
  }

  public getTaskInfoById(id: number): UploadTaskInfo | undefined {
    return this.taskCache.get(id);
  }

  public getTaskInfoByFileInfo(url: string, filePath: string, uploadFileName: string): UploadTaskInfo | undefined {
    for (const value of this.taskCache.values()) {
      if (value.url === url && value.filePath === filePath && value.uploadFileName === uploadFileName) {
        return value;
      }
    }
    return undefined;
  }

  public setTaskInfo(taskInfo: UploadTaskInfo) {
    if (!taskInfo.id) {
      Logger.error(LoggerConstants.CACHE, 'TaskInfo has no valid id');
      throw new CacheError('Failed to store taskInfo in cache', CacheErrorType.INSERT_TASK_INFO_ERROR);
    }
    this.taskCache.set(taskInfo.id, taskInfo);
  }

  public removeTaskInfoById(id: number): boolean {
    return this.taskCache.delete(id);
  }
}
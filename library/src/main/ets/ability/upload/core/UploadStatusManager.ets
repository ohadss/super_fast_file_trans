/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UploadTaskMetadata } from './UploadTaskMetadata';
import { UploadStatusType } from '../common/UploadStatusType';
import { UploadEventConstants } from '../common/UploadConstants';
import { UploadStatusError, UploadStatusErrorType } from '../../../common/exception/upload/UploadStatusError';
import { UploadEventManager } from './UploadEventManager';
import LoggerConstants from '../../../common/constants/LoggerConstants';
import Logger from '../../../common/utils/Logger';

export class UploadStatusManager {
  private static instance: UploadStatusManager;
  private statusSetFunctionMap: Map<UploadStatusType, (downloadTaskMetadata: UploadTaskMetadata) => void> = new Map();
  public uploadingTaskCount: number;

  private constructor() {
    this.uploadingTaskCount = 0;
    this.statusSetFunctionMap.set(UploadStatusType.INITIALIZED,
      (downloadTaskMetadata: UploadTaskMetadata): void => this.setInitialized(downloadTaskMetadata));
    this.statusSetFunctionMap.set(UploadStatusType.UPLOADING,
      (downloadTaskMetadata: UploadTaskMetadata): void => this.setUploading(downloadTaskMetadata));
    this.statusSetFunctionMap.set(UploadStatusType.PAUSED,
      (downloadTaskMetadata: UploadTaskMetadata): void => this.setPaused(downloadTaskMetadata));
    this.statusSetFunctionMap.set(UploadStatusType.CANCELED,
      (downloadTaskMetadata: UploadTaskMetadata): void => this.setCanceled(downloadTaskMetadata));
    this.statusSetFunctionMap.set(UploadStatusType.COMPLETED,
      (downloadTaskMetadata: UploadTaskMetadata): void => this.setCompleted(downloadTaskMetadata));
    this.statusSetFunctionMap.set(UploadStatusType.FAILED,
      (downloadTaskMetadata: UploadTaskMetadata): void => this.setFailed(downloadTaskMetadata));
  }

  public static getInstance() {
    if (!UploadStatusManager.instance) {
      UploadStatusManager.instance = new UploadStatusManager();
    }
    return UploadStatusManager.instance;
  }

  public setStatus(status: UploadStatusType, downloadTaskMetadata: UploadTaskMetadata) {
    const func = this.statusSetFunctionMap.get(status);
    if (func) {
      func(downloadTaskMetadata);
    } else {
      throw new UploadStatusError('Target status does not exist', UploadStatusErrorType.NOT_EXIST_ERROR);
    }
  }

  private setInitialized(uploadTaskMetadata: UploadTaskMetadata) {
    UploadEventManager.getInstance().emitEvent(UploadEventConstants.STOP_EVENT, uploadTaskMetadata.id);
    UploadEventManager.getInstance().emitEvent(UploadEventConstants.CANCEL_EVENT, uploadTaskMetadata.id);
    UploadEventManager.getInstance().offEvent(UploadEventConstants.FAIL_EVENT, uploadTaskMetadata.id);
    uploadTaskMetadata.status = UploadStatusType.INITIALIZED;
  }

  private setUploading(uploadTaskMetadata: UploadTaskMetadata) {
    if (uploadTaskMetadata.status === UploadStatusType.UPLOADING) {
      Logger.warn(LoggerConstants.STATUS, 'Failed to resume upload while uploading');
      throw new UploadStatusError('Failed to resume upload while uploading',
        UploadStatusErrorType.RESUME_WHILE_UPLOADING_ERROR);
    }

    if (uploadTaskMetadata.status === UploadStatusType.COMPLETED) {
      Logger.warn(LoggerConstants.STATUS, 'Upload task is already completed');
      throw new UploadStatusError('Upload task is already completed',
        UploadStatusErrorType.RESUME_AFTER_COMPLETED_ERROR);
    }

    uploadTaskMetadata.status = UploadStatusType.UPLOADING;
  }

  private setPaused(uploadTaskMetadata: UploadTaskMetadata) {
    // 从上传态进入取消态，上传计数减一
    if (uploadTaskMetadata.status === UploadStatusType.UPLOADING) {
      UploadEventManager.getInstance().emitEvent(UploadEventConstants.STOP_EVENT, uploadTaskMetadata.id);
      UploadEventManager.getInstance().offEvent(UploadEventConstants.CANCEL_EVENT, uploadTaskMetadata.id);
      UploadEventManager.getInstance().offEvent(UploadEventConstants.FAIL_EVENT, uploadTaskMetadata.id);
      uploadTaskMetadata.status = UploadStatusType.PAUSED;
    } else {
      Logger.warn(LoggerConstants.STATUS, 'Upload task is only allowed to be paused while uploading');
      throw new UploadStatusError('Upload task is only allowed to be paused while uploading',
        UploadStatusErrorType.SET_PAUSED_ERROR);
    }
  }

  private setCanceled(uploadTaskMetadata: UploadTaskMetadata) {
    // 取消状态再次取消无意义，其他状态均可随时取消
    if (uploadTaskMetadata.status === UploadStatusType.CANCELED) {
      Logger.warn(LoggerConstants.STATUS, 'Download task is already canceled');
      throw new UploadStatusError('Download task is already canceled', UploadStatusErrorType.SET_CANCELED_ERROR);
    }
    UploadEventManager.getInstance().emitEvent(UploadEventConstants.CANCEL_EVENT, uploadTaskMetadata.id);
    UploadEventManager.getInstance().offEvent(UploadEventConstants.STOP_EVENT, uploadTaskMetadata.id);
    UploadEventManager.getInstance().offEvent(UploadEventConstants.FAIL_EVENT, uploadTaskMetadata.id);
    uploadTaskMetadata.status = UploadStatusType.CANCELED;
  }

  private setFailed(uploadTaskMetadata: UploadTaskMetadata) {
    // 任务上传完成后不会失败，其他状态均可进入失败状态
    if (uploadTaskMetadata.status === UploadStatusType.COMPLETED) {
      Logger.warn(LoggerConstants.STATUS, 'Upload task is already completed');
      throw new UploadStatusError('Upload task is already completed', UploadStatusErrorType.SET_FAILED_ERROR);
    }
    UploadEventManager.getInstance().emitEvent(UploadEventConstants.FAIL_EVENT, uploadTaskMetadata.id);
    UploadEventManager.getInstance().offEvent(UploadEventConstants.STOP_EVENT, uploadTaskMetadata.id);
    UploadEventManager.getInstance().offEvent(UploadEventConstants.CANCEL_EVENT, uploadTaskMetadata.id);
    uploadTaskMetadata.status = UploadStatusType.FAILED;
  }

  private setCompleted(uploadTaskMetadata: UploadTaskMetadata) {
    // 上传过程正常结束，进入上传完成态
    if (uploadTaskMetadata.status === UploadStatusType.UPLOADING) {
      UploadEventManager.getInstance().offEvent(UploadEventConstants.FAIL_EVENT, uploadTaskMetadata.id);
      UploadEventManager.getInstance().offEvent(UploadEventConstants.STOP_EVENT, uploadTaskMetadata.id);
      UploadEventManager.getInstance().offEvent(UploadEventConstants.CANCEL_EVENT, uploadTaskMetadata.id);
      uploadTaskMetadata.status = UploadStatusType.COMPLETED;
    } else {
      Logger.warn(LoggerConstants.STATUS, 'Upload task is about to be completed abnormally');
      throw new UploadStatusError('Upload task is about to be completed abnormally',
        UploadStatusErrorType.SET_COMPLETED_ERROR);
    }
  }
}

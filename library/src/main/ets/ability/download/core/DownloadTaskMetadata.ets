/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { rcp } from '@kit.RemoteCommunicationKit';
import { DownloadTaskConstants } from '../common/DownloadConstants';
import { DownloadStatusType } from '../common/DownloadStatusType';
import { DownloadConfig } from '../api/DownloadConfig';
import { DownloadListener } from '../api/DownloadListener';
import { SecurityConfig } from '../../common/SecurityConfig';

export class DownloadTaskMetadata {
  public id: number;
  public url: string;
  public fileName: string;
  public fileDir: string;
  public fileSize: number;
  public etag: string;
  public lastModified: string;
  public maxRetries: number;
  public retryInterval: number;
  public concurrency?: number;
  public connectTimeout: number;
  public transferTimeout: number;
  public inactivityTimeout: number;
  public isResumable: boolean;
  public isOverwrite: boolean;
  public requestHeaders?: rcp.RequestHeaders;
  public listener?: DownloadListener;
  public status: DownloadStatusType;
  public securityConfig?: SecurityConfig;

  constructor(config: DownloadConfig, listener?: DownloadListener) {
    this.id = DownloadTaskConstants.DEFAULT_ID;
    this.url = config.url;
    this.fileName = config.fileName;
    this.fileDir = config.fileDir ?? getContext(this).filesDir;
    this.fileSize = DownloadTaskConstants.DEFAULT_ZERO_FILE_SIZE;
    this.etag = DownloadTaskConstants.DEFAULT_NULL_ETAG;
    this.lastModified = DownloadTaskConstants.DEFAULT_NULL_LAST_MODIFIED;
    this.maxRetries = config.maxRetries ?? DownloadTaskConstants.DEFAULT_RETRIES;
    this.retryInterval = config.retryInterval ?? DownloadTaskConstants.DEFAULT_RETRY_INTERVAL;
    this.concurrency = config.concurrency;
    this.connectTimeout = config.connectTimeout ?? DownloadTaskConstants.DEFAULT_CONNECT_TIME;
    this.transferTimeout = config.transferTimeout ?? DownloadTaskConstants.DEFAULT_TRANSFER_TIME;
    this.inactivityTimeout = config.inactivityTimeout ?? DownloadTaskConstants.DEFAULT_INACTIVITY_TIME;
    this.isResumable = config.isBreakpointResume ?? DownloadTaskConstants.DEFAULT_IS_RESUMABLE;
    this.isOverwrite = config.isOverwrite ?? DownloadTaskConstants.DEFAULT_IS_OVERWRITE;
    this.requestHeaders = config.requestHeaders ? this.convertHeader(config.requestHeaders) :
    DownloadTaskConstants.DEFAULT_REQUEST_HEADERS;
    this.listener = listener ?? DownloadTaskConstants.DEFAULT_LISTENER;
    this.status = DownloadStatusType.INITIALIZED;
    this.securityConfig = config.securityConfig;
  }

  private convertHeader(requestHeaders: Record<string, string | string[] | undefined>): rcp.RequestHeaders {
    const rcpRequestHeaders: rcp.RequestHeaders = {};
    Object.keys(requestHeaders).forEach((key) => {
      rcpRequestHeaders[key] = requestHeaders[key];
    })
    return rcpRequestHeaders;
  }
}
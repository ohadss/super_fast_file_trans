/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { cryptoFramework } from '@kit.CryptoArchitectureKit';
import { buffer, util } from '@kit.ArkTS';

export class EncryptUtils {
  public static async getBase64Md(text: string, algorithm: string): Promise<string> {
    const md = cryptoFramework.createMd(algorithm);
    await md.update({ data: new Uint8Array(buffer.from(text, 'utf-8').buffer) });
    const mdResult = await md.digest();
    const base = new util.Base64Helper();
    return base.encodeToStringSync(mdResult.data);
  }
}